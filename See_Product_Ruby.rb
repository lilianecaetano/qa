require "json"
require "selenium-webdriver"
require "rspec"
include RSpec::Expectations

describe "AddToCart" do

  before(:each) do
    @driver = Selenium::WebDriver.for :firefox
    @base_url = "http://www.williams-sonoma.com/"
    @accept_next_alert = true
    @driver.manage.timeouts.implicit_wait = 30
    @verification_errors = []
  end
  
  after(:each) do
    @driver.quit
    @verification_errors.should == []
  end
  
  it "test_add_to_cart" do
    @driver.get(@base_url + "/")
    verify { (@driver.find_element(:link, "Cookware Sets").text).should == "Cookware Sets" }
    !60.times{ break if (element_present?(:link, "Cookware Sets") rescue false); sleep 1 }
    @driver.find_element(:link, "Cookware Sets").click
    !60.times{ break if (element_present?(:css, "img.product-thumb") rescue false); sleep 1 }
    @driver.find_element(:css, "img.product-thumb").click
    !60.times{ break if (element_present?(:xpath, "//div[@id='pip']/div/div[7]/div[2]/div[2]/section/div/div/fieldset/button") rescue false); sleep 1 }
    verify { (@driver.find_element(:xpath, "//div[@id='pip']/div/div[7]/div[2]/div[2]/section/div/div/fieldset/button").text).should == "Add to Cart" }
    @driver.find_element(:xpath, "//div[@id='pip']/div/div[7]/div[2]/div[2]/section/div/div/fieldset/button").click
    !60.times{ break if (element_present?(:id, "anchor-btn-checkout") rescue false); sleep 1 }
    verify { (@driver.find_element(:id, "anchor-btn-checkout").text).should == "Checkout" }
    @driver.find_element(:id, "anchor-btn-checkout").click
    @driver.get(@base_url + "/shoppingcart/")
    verify { (@driver.find_element(:xpath, "(//input[@type='image'])[4]").text).should == "" }
    @driver.find_element(:xpath, "(//input[@type='image'])[4]").click
  end
  
  def element_present?(how, what)
    ${receiver}.find_element(how, what)
    true
  rescue Selenium::WebDriver::Error::NoSuchElementError
    false
  end
  
  def alert_present?()
    ${receiver}.switch_to.alert
    true
  rescue Selenium::WebDriver::Error::NoAlertPresentError
    false
  end
  
  def verify(&blk)
    yield
  rescue ExpectationNotMetError => ex
    @verification_errors << ex
  end
  
  def close_alert_and_get_its_text(how, what)
    alert = ${receiver}.switch_to().alert()
    alert_text = alert.text
    if (@accept_next_alert) then
      alert.accept()
    else
      alert.dismiss()
    end
    alert_text
  ensure
    @accept_next_alert = true
  end
end
